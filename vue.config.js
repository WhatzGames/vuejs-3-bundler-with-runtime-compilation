// vue.config.js
module.exports = {
  // disable hashes in filenames
  filenameHashing: false,
  // delete HTML related webpack plugins
  chainWebpack: config => {
    // config.plugins.delete('html')
    // config.plugins.delete('preload')
    // config.plugins.delete('prefetch')
    config
      .entry("app")
      .clear()
      .add("./components/main.ts")
      .end();
    config.resolve.alias
      .set('vue$', 'vue/dist/vue.esm-bundler.js')
    config.resolve.alias
      .set('@', __dirname + '/components');
  }
}
