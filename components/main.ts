import { createApp, defineAsyncComponent } from 'vue';

createApp({})
  .component('test-element', defineAsyncComponent(() => import('@/TestElement/TestElement.vue')))
  .mount('#app');
